# --------------------------------------------------------------------------------
# main.sh
#
# Author:  Ozymandias
#          [ef3add5f32de7d98ead2e7bb6dfad933298fc99e6b910fa3678e3ba54e7303df]
# Github:  pirates-of-silicon-hills/VoightKampff
# Date:    November 2019-July 2020
# License: MIT 
#
#
#     I'm not a comic book villain. Do you seriously
#     think I would explain my master stroke to you 
#     if there were even the slightest possibility 
#     you could affect the outcome?           
#
#                       I triggered it 35 minutes ago
#                       				-Adrian Veidt
#
# --------------------------------------------------------------------------------



echo "████████████████"
echo "██          ██  ██" 
echo "██          ██    ██"
echo "██  ████  ██████  ██"
echo "██  ████  ██████████"
echo "██        ██████  ██"
echo "██                ██"
echo "██                ██"
echo "██  ██        ██  ██"
echo "██  ████████████  ██"
echo "██                ██"  
echo "████████████████████"
echo ""
echo "PIRATES OF SILICON HILLS 2020"
echo "MIT License"

. ./checkScore.sh

for orbit in {0..1000}
do
	for dns in {0..19}
	do
		echo "Region: " $dns
		./switchDNS.sh $dns
		get_score
		score=$?
		if [ $score -ge 3 ]; then
			./setWindow_hackerNews.sh
			./hackerNews.sh 
			time ./main.sh -97 46 0 $score # reddit -65 60 0
			#sleep 5
			#get_score
		else
			echo "Skipping due to low score on V3 captcha"
		fi
	done
done

