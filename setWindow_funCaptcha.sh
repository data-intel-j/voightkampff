echo ">>setWindow.sh"
xdotool windowsize `xdotool search --onlyvisible -name firefox` 2280 1600
xdotool windowmove `xdotool search --onlyvisible -name firefox` 0 0
sleep 1
xdotool mousemove 2240 80 click 1 #Close Firefox properly
sleep 2
xdotool mousemove 4000 2000 # make sure mouse is not there when we take screenshot to check for firefox errors
firefox "https://funcaptcha.com/fc/api/nojs/?pkey=69A21A01-CC7B-B9C6-0F9A-E7FA06677FFC" &
sleep 5
n=$RANDOM
x=$((2780-$n%60))
y=$((2100-$n%60))
echo $x $y
xdotool windowsize `xdotool search --onlyvisible -name firefox` $x $y  # 2280 1600
xdotool windowmove `xdotool search --onlyvisible -name firefox` 0 0
sleep 2