echo ">>reddit.sh"

xdotool windowsize `xdotool search --onlyvisible -name firefox` 2280 1600
xdotool windowmove `xdotool search --onlyvisible -name firefox` 0 0

# Some regions ask if you want to receice Emails from Reddit
maim -g 1000x100+320+680 getEmails.png
STR=$(tesseract getEmails.png stdout)
SUB='to get emails about cool stuff '
if [[ "$STR" == *"$SUB"* ]]; then
	echo "Region with email radio button"
	xdotool mousemove 500 1250 click 1                      # Click on field for email
	xdotool type worlds.smartest.man.2020@gmail.com
	sleep 1
  	xdotool mousemove 500 1370 click 1
	xdotool mousemove 500 1370 click 1
else
	xdotool mousemove 500 1220 click 1                      # Click on field for email
	xdotool type worlds.smartest.man.2020@gmail.com
	sleep 1
	xdotool mousemove 500 1340 click 1
	xdotool mousemove 500 1340 click 1
fi

sleep 2
xdotool mousemove 500 650 click 1                      # Click on username field
xdotool type xanderTheGreat032688                      # Enter username
sleep 0.5
xdotool mousemove 1000 650 click 1                     # Click outside so that saved passwords dissapear
xdotool mousemove 500 800 click 1                      # Click in password field
xdotool type RamessesII
sleep 0.5
xdotool mousemove 1000 800 click 1                     # Click outside so that saved passwords dissapears
sleep 1

n=$RANDOM
x=$((2780-$n%60))
y=$((2100-$n%60))
echo $x $y

xdotool windowsize `xdotool search --onlyvisible -name firefox` $x $y  #2280 1600
xdotool windowmove `xdotool search --onlyvisible -name firefox` 0 0
sleep 1

waitForPuzzle=0
puzzleLoaded=0
while [ $puzzleLoaded -eq 0 ] && [ $waitForPuzzle -le 10 ]
do
	xdotool mousemove 1000 1000 click 1                    			   # Get focus	
	maim -g 500x200+50+840 boxLoaded.png
	STR=$(tesseract boxLoaded.png stdout)	

	SUB='not a robot'
	if [[ "$STR" == *"$SUB"* ]]; then
	  	puzzleLoaded=1
	  	echo "Puzzle Loaded"
  	else
		echo "Waiting for tick to load"
	fi
	((waitForPuzzle=waitForPuzzle+1))
	sleep 0.5
done

echo "Box loaded" 

xdotool mousemove 100 980 click 1                                     # Click on tick
sleep 2







